﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace RegEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Match match;
            string URLaddress = "http://www.mail.ru/";
            Uri customURL = new Uri(URLaddress);
            Console.WriteLine("Parsing URL " + URLaddress + "...");
            string pattern = @"href\s*=\s*['""]?(?<url>[^""'>\s]+)['"">\s]";

            string html = new WebClient().DownloadString(customURL);

            match = Regex.Match(html, pattern, RegexOptions.IgnoreCase);

            Console.WriteLine("Below are found addresses :");

            while (match.Success)
            {
                Console.WriteLine($"{match.Groups["url"]} at {match.Groups["url"].Index}");
                match = match.NextMatch();
            }

            Console.ReadKey();
        }
    }
}
